const url = "https://opensky-network.org/api/states/all?lamin=45.8389&lomin=5.9962&lamax=47.8229&lomax=10.5226"
var markers = [];
var updateIntervall;

// Initialisierung der Map
var mymap = L.map('mapid').setView([46.805119, 8.380210], 8)

// Anbindung der Map an Mapbox
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    //id: 'mapbox/satellite-v9',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoic3RtOTciLCJhIjoiY2tpcDA3bGZuMDI1OTJzcDlrb3Q5bGp3NyJ9.7J5EFUdN534zYPaYDQO47g'
}).addTo(mymap);

//Begrenzung einzeichnen
var polygon = L.polygon([
  [45.8389, 5.9962],
  [45.8389, 10.5226],
  [47.8229, 10.5226],
  [47.8229, 5.9962],
],
{
  color: 'red',
  fillOpacity: 0,
}
).addTo(mymap);

//Flieger als Marker definieren
var flieger = L.icon({
  iconUrl: './pictures/plane.png',

  iconSize: [30,30],
  iconAnchor: [15,15],
  popupAnchor: [-3,-3],
});


//Inital Marker und Interval erzeugen
async function onLoad(){
  var newdata = []
  newdata = await getNewData();
  addMarker(newdata);
  updateIntervall = setInterval(updateMarker,10000);
}

//Aktuelle Flugdaten abrufen
async function getNewData() {
  var newdata = []
  await fetch(url)
  .then(response =>
    response.json()
  )
  .then(data => {
    newdata = data.states;
  })
  .catch((err) => {
    console.log(err);
  });
  return(newdata)
}

//Erstellt für jeden Flug im Array einen Marker
function addMarker(flights) {
	for (var i of flights) {
    var newmarker = []
    newmarker.push(i[0]);
		newmarker.push(L.marker([i[6], i[5]],{icon: flieger, rotationAngle: i[10]+270}).addTo(mymap)
    .bindPopup('<h3>Flug: <a href="https://de.flightaware.com/live/flight/' + i[1]+ '" target="_blank">'+i[1]+'</a> </h3>'+
    '<h4>Flughöhe: '+i[7]*1+'m</h4> <h4>Geschwindigkeit: '+Math.round(i[9]*3.6)+'km/h', {closeButton: false}));
    markers.push(newmarker)
	}
}

//Updatet die Aktuellen Marker
async function updateMarker() {
  newdata = await getNewData();
  var newflights = [];
  var updated = false;
  for (var i of newdata) {
    var updated = false;
    for (var a of markers) {
      if (a[0] == i[0]) {
        //Position aktueller Flüge updaten
        a[1].setLatLng([i[6], i[5]]);
        updated = true;
      }
    }
    if(!updated){newflights.push(i);}
  }
  //Neue Flüge hinzufügen
  addMarker(newflights);
  //Alte Flüge löschen
  removeOld(newdata);
}

//Suchen und löschen von alten Markern
function removeOld(data) {
  var old = true;
  var oldmarkers = [];
  for (var i of markers) {
    for (var a of data) {
      if (a[0] == i[0]) {
        old = false;
      }
    }
    if (old) {
      //Alte Marker von der Map löschen
      mymap.removeLayer(i[1]);
      oldmarkers.push(i);
    }
    old = true;
  }
  //Alte Marker aus Liste löschen
  markers = markers.filter(item => !oldmarkers.includes(item))
}
